*** Settings ***
Resource         ../resource/Resource.robot
Test Setup       Abrir navegador
Test Teardown    Fechar navegador

### SETUP ele roda keyword antes da suite ou antes de um teste
### TEARDOWN ele roda keyword depois de uma suite ou um teste


*** Test Case ***
Caso de Teste 01: Pesquisar produto existente
   Verificar a página home do site
   Digitar o nome de produto "Blouse" no campo de pesquisa
   Clicar no botão Pesquisar
   Conferir se o produto "Blouse" foi listado no site1

Caso de teste 02: Pesquisar produto não existente
   Verificar a página home do site
   Digitar o nome de produto "Brouse" no campo de pesquisa
   Clicar no botão Pesquisar
   Conferir mensagem de erro "No results were found for your search "Brouse""

Caso de teste 03: Listar Produtos
   Verificar a página home do site
   Passar o mouse por cima da categoria "Women"
   Clicar na sub categoria "Summer Dresses"

Caso de teste 04: Adicionar Produtos no Carrinho
   Verificar a página home do site
   Digitar o nome de produto "T-shirt" no campo de pesquisa
   Clicar no botão Pesquisar
   Conferir se o produto "T-shirt" foi listado no site2
   Clicar no botão "Add to cart" do produto
   Clicar no botão "Proceed to checkout"

# Caso de teste 05: Remover Produtos
    Acessar página home
    Verificar a página home do site
    Clicar no ícone carrinho de compras
    Clicar no botão de remoção de produtos do carrinho
    Conferir mensagem de "Your shopping cart is empty."

Caso de teste 06: Adicionar Cliente
      Verificar a página home do site
      Clicar no botão superior direito “Sign in”
      Inserir um e-mail válido
      Clicar no botão "Create na account"
      Preencher os campos obrigatórios
      Clicar em "Register" para finalizar o cadastro



# *** Keywords ***
