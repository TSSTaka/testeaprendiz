*** Settings ***
Library      SeleniumLibrary

### COMANDO PARA O TESTE: robot -d ./results tests\TestSitePractice.robot

*** Variables ***
${URL}        http://automationpractice.com/index.php
${BROWSER}    chrome

*** Keywords ***
### Setup e Teardown
Abrir navegador
    Open Browser  http://automationpractice.com/index.php  ${BROWSER}

Fechar navegador
    Close Browser

### Passo-a-Passo
Acessar página home
    Click Element                   xpath=//*[@id="columns"]/div[1]/a

Verificar a página home do site
    Title Should Be                 My Store

Digitar o nome de produto "${PRODUTO}" no campo de pesquisa
    Input Text                      name=search_query  ${PRODUTO}

Clicar no botão Pesquisar
    Click Element                   name=submit_search

Conferir se o produto ${PRODUTO} foi listado no site1
    Wait Until Element Is Visible   css=#center_column > h1
    Title Should Be                 Search - My Store
    Page Should Contain Image       xpath=//*[@id="center_column"]//*[@src='http://automationpractice.com/img/p/7/7-home_default.jpg']
    Page Should Contain Link        xpath=//*[@id="center_column"]//a[@class="product-name"][contains(text(),${PRODUTO})]

Conferir se o produto ${PRODUTO} foi listado no site2
    Wait Until Element Is Visible   css=#center_column > h1
    Title Should Be                 Search - My Store
    Page Should Contain Image       xpath=//*[@id="center_column"]//*[@src='http://automationpractice.com/img/p/1/1-home_default.jpg']
    Page Should Contain Link        xpath=//*[@id="center_column"]//a[@class="product-name"][contains(text(),${PRODUTO})]

Conferir mensagem de erro "${MENSAGEM_ALERTA}"
    Wait Until Element Is Visible   //*[@id="center_column"]//p[@class="alert alert-warning"]
    Element Text Should Be          //*[@id="center_column"]//p[@class="alert alert-warning"]  ${MENSAGEM_ALERTA}

Conferir mensagem de "${MENSAGEM_ALERTA}"
    Wait Until Element Is Visible   //*[@id="center_column"]//p[@class="alert alert-warning"]
    Element Text Should Be          //*[@id="center_column"]//p[@class="alert alert-warning"]  ${MENSAGEM_ALERTA}

Passar o mouse por cima da categoria "${CATEGORIA}"
    Mouse Over                      css=#block_top_menu > ul > li:nth-child(1) > a
    ${TEXTOPRODUTO1}  Get Text      //*[@id="block_top_menu"]/ul/li[1]/ul/li[1]/ul/li[1]/a
    Should Be Equal As Strings      ${TEXTOPRODUTO1}    T-shirts
    ${TEXTOPRODUTO2}  Get Text      //*[@id="block_top_menu"]/ul/li[1]/ul/li[2]/ul/li[3]/a
    Should Be Equal As Strings      ${TEXTOPRODUTO2}    Summer Dresses
    Page Should Contain Element     xpath=//*[@id="block_top_menu"]/ul/li[1]/ul

Clicar na sub categoria "Summer Dresses"
    Click Element                   css=#block_top_menu > ul > li:nth-child(1) > ul > li:nth-child(2) > ul > li:nth-child(3) > a
    Wait Until Element Is Visible   xpath=//*[@id="center_column"]/div[1]/div/div/span
    Page Should Contain Image       xpath=//*[@id="center_column"]/ul/li[1]/div/div[1]/div/a[1]/img
    Page Should Contain Image       xpath=//*[@id="center_column"]/ul/li[2]/div/div[1]/div/a[1]/img

Clicar no botão "Add to cart" do produto
    Click Element                   xpath=//*[@id="center_column"]/ul/li/div/div[2]/div[2]/a[1]
    Wait Until Element Is Visible   xpath=//*[@id="layer_cart"]/div[1]/div[2]/div[4]/a

Clicar no botão "Proceed to checkout"
    Click Element                   xpath=//*[@id="layer_cart"]/div[1]/div[2]/div[4]/a
    Page Should Contain Element     xpath=//*[@id="cart_title"][contains(text(),"Shopping-cart summary")]
    Page Should Contain Element     xpath=//*[@id="cart_summary"]/thead/tr/th[1]
    Page Should Contain Element     xpath=//*[@id="cart_summary"]/thead/tr/th[2]
    Page Should Contain Image       xpath=//*[@id="product_1_1_0_0"]/td[1]/a/img

Clicar no ícone carrinho de compras
    Click Element                   xpath=//*[@id="header"]/div[3]/div/div/div[3]/div/a
    Page Should Contain Element     xpath=//*[@id="cart_title"][contains(text(),"Shopping-cart summary")]
    Page Should Contain Element     xpath=//*[@id="cart_summary"]/thead/tr/th[1]
    Page Should Contain Element     xpath=//*[@id="cart_summary"]/thead/tr/th[2]

Clicar no botão de remoção de produtos do carrinho
    Click Element                   xpath=//*[@id="1_1_0_0"]

Clicar no botão superior direito “Sign in”
    Click Element                   xpath=//*[@id="header"]/div[2]/div/div/nav/div[1]/a
    Wait Until Element Is Visible   xpath=//*[@id="center_column"]/h1
    Page Should Contain Element     xpath=//*[@id="create-account_form"]/h3

Inserir um e-mail válido
    Input Text                      id=email_create   pp@exemple.com
    Press Keys                      none  TAB
    Wait Until Element Is Visible   css=#create-account_form > div > div.form-group.form-ok

Clicar no botão "Create na account"
    Click Element                   xpath=//*[@id="SubmitCreate"]
    Wait Until Element Is Visible   xpath=//*[@id="noSlide"]/h1
    Wait Until Element Is Visible   xpath=//*[@id="account-creation_form"]//h3[@class="page-subheading"][contains(text(),"Your personal information")]

Preencher os campos obrigatórios
    Input Text                      name=customer_firstname  Thiago
    Input Text                      name=customer_lastname   dos Santos
    Input Text                      name=passwd              11111
    Input Text                      name=address1            Rua teste
    Input Text                      name=city                New York
    Select From List By Value       name=id_state            32
    Input Text                      name=postcode            00000
    Select From List By Value       name=id_country          21
    Input Text                      name=phone_mobile        9999999

Clicar em "Register" para finalizar o cadastro
    Click Button                    xpath=//*[@id="submitAccount"]
    Element Text Should Be          //*[@id="center_column"]/p[@class="info-account"]   Welcome to your account. Here you can manage all of your personal information and orders.
